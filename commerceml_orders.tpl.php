<?php print '<?xml version="1.0" encoding="windows-1251"?>'; ?>
<КоммерческаяИнформация
  ВерсияСхемы="2.05"
  ФорматДаты="ДФ=yyyy-MM-dd; ДЛФ=DT"
  ФорматВремени="ДФ=ЧЧ:мм:сс; ДЛФ=T"
  РазделительДатаВремя="T"
  ФорматСуммы="ЧЦ=18; ЧДЦ=2; ЧРД=."
  ФорматКоличества="ЧЦ=18; ЧДЦ=2; ЧРД=."
  ДатаФормирования="<?php print date('Y-m-d'); ?>T<?php print date('H:m:s'); ?>">
<?php foreach ($orders as $oid => $order) { ?>
  <Документ>
    <Ид><?php print $order->order_id; ?></Ид>
    <Номер><?php print $order->order_number; ?></Номер>
    <Дата><?php print date('Y-m-d', $order->created); ?></Дата>
    <ХозОперация>Заказ товара</ХозОперация>
    <Роль>Продавец</Роль>
    <Валюта>руб.</Валюта>
    <Курс>1</Курс>
    <Сумма><?php print number_format($order->commerce_order_total['und'][0]['amount'] / 100, 2, '.', ''); ?></Сумма>
    <Контрагенты>
      <Контрагент>
        <Ид><?php print $order->commerceml_user_id; ?></Ид>
        <Наименование><?php $name = commerceml_token($order, 'name'); print $name ? $name : $order->commerceml_user_id; ?></Наименование>
        <ПолноеНаименование><?php print $name ? $name : $order->commerceml_user_id; ?></ПолноеНаименование>
        <РабочееНаименование><?php print $name ? $name : $order->commerceml_user_id; ?></РабочееНаименование>
        <ОфициальноеНаименование><?php print $name ? $name : $order->commerceml_user_id; ?></ОфициальноеНаименование>
        <АдресРегистрации>
          <Вид>Адрес доставки</Вид>
          <Представление><?php print commerceml_token($order, 'shipping_address'); ?></Представление>
        </АдресРегистрации>
        <ИНН><?php print commerceml_token($order, 'inn'); ?></ИНН>
        <КПП><?php print commerceml_token($order, 'kpp'); ?></КПП>
        <ЮридическийАдрес>
          <Представление><?php print commerceml_token($order, 'legal_address'); ?></Представление>
        </ЮридическийАдрес>
<?php
$bank_account_number = commerceml_token($order, 'bank_account_number');
if ($bank_account_number) { ?>
        <РасчетныеСчета>
          <РасчетныйСчет>
            <НомерСчета><?php print $bank_account_number; ?></НомерСчета>
            <Банк>
              <БИК><?php print commerceml_token($order, 'bank_bic'); ?></БИК>
            </Банк>
          </РасчетныйСчет>
        </РасчетныеСчета>
<?php } ?>
        <Контакты>
<?php
$mail = commerceml_token($order, 'mail');
if ($mail) { ?>
          <Контакт>
            <Тип>Почта</Тип>
            <Значение><?php print $mail; ?></Значение>
          </Контакт>
<?php }
$phone = commerceml_token($order, 'phone');
if ($phone) { ?>
?>
          <Контакт>
            <Тип>Телефон</Тип>
            <Значение><?php print $phone; ?></Значение>
          </Контакт>
<?php } ?>
        </Контакты>
        <Роль>Покупатель</Роль>
      </Контрагент>
    </Контрагенты>
    <Время><?php print date('H:m:s', $order->created); ?></Время>
    <Комментарий><?php print commerceml_token($order, 'comment'); ?></Комментарий>
    <Товары>
<?php
foreach ($order->commerceml_line_items as $li) {
  if ($li->type == 'shipping') {
    $id = 'ORDER_DELIVERY';
    $title = 'Доставка заказа';
    $order_type = $li->data['shipping_service']['name'];
    $order_label = $li->data['shipping_service']['display_title'];
    $nomenclature_type = 'Услуга';
  }
  else {
    $product_id = $li->commerce_product['und'][0]['product_id'];
    $product = $order->commerceml_products[$product_id];
    $id = $product->commerceml_guid;
    $title = $product->title;
    $nomenclature_type = 'Товар';
  }
  $price = number_format((double)$li->commerce_unit_price['und'][0]['amount'] / 100, 2, '.', '');
  $total = number_format((double)($li->quantity * $price), 2, '.', '');
  $quantity = $li->quantity;
?>
      <Товар>
        <Ид><?php print $id; ?></Ид>
        <ИдКаталога>DRUPAL-CATALOG</ИдКаталога>
        <Наименование><?php print $title; ?></Наименование>
        <ЦенаЗаЕдиницу><?php print $price; ?></ЦенаЗаЕдиницу>
        <ЕдиницаИзмерения>шт</ЕдиницаИзмерения>
        <БазоваяЕдиница>
          <Код>796</Код>
          <НаименованиеПолное>Штука</НаименованиеПолное>
        </БазоваяЕдиница>
        <ЗначенияРеквизитов>
          <ЗначениеРеквизита>
            <Наименование>ВидНоменклатуры</Наименование>
            <Значение><?php print $nomenclature_type; ?></Значение>
          </ЗначениеРеквизита>
          <ЗначениеРеквизита>
            <Наименование>ТипНоменклатуры</Наименование>
            <Значение><?php print $nomenclature_type; ?></Значение>
          </ЗначениеРеквизита>
        </ЗначенияРеквизитов>
        <Количество><?php print $quantity; ?></Количество>
        <Сумма><?php print $total; ?></Сумма>
      </Товар>
<?php
}
?>
    </Товары>
    <ЗначенияРеквизитов>
      <ЗначениеРеквизита>
        <Наименование>Отменен</Наименование>
        <Значение>false</Значение>
      </ЗначениеРеквизита>
      <ЗначениеРеквизита>
        <Наименование>Финальный статус</Наименование>
        <Значение>false</Значение>
      </ЗначениеРеквизита>
      <ЗначениеРеквизита>
        <Наименование>Статус заказа</Наименование>
        <Значение><?php print $order->commerceml_status_title; ?></Значение>
      </ЗначениеРеквизита>
      <ЗначениеРеквизита>
        <Наименование><?php print $order->commerceml_status; ?></Наименование>
        <Значение>true</Значение>
      </ЗначениеРеквизита>
      <ЗначениеРеквизита>
        <Наименование>Сайт</Наименование>
        <Значение>Drupal</Значение>
      </ЗначениеРеквизита>
      <ЗначениеРеквизита>
        <Наименование>Метод оплаты</Наименование>
        <Значение>Наличными</Значение>
      </ЗначениеРеквизита>
      <?php if (!empty($order_type)) { ?>
        <ЗначениеРеквизита>
          <Наименование>Способ доставки</Наименование>
          <Значение><?php print $order_label; ?></Значение>
        </ЗначениеРеквизита>
        <ЗначениеРеквизита>
          <Наименование>Адрес доставки</Наименование>
          <Значение><?php print commerceml_token($order, 'shipping_address'); ?></Значение>
        </ЗначениеРеквизита>
      <?php } ?>
    </ЗначенияРеквизитов>
  </Документ>
<?php } ?>
</КоммерческаяИнформация>