<?php

/**
 * Form callback: edit CommerceML settings.
 */
function commerceml_settings_form($form, &$form_state) {
  // Path to store CommerceML import files.
  $form['commerceml_file_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('File limit'),
      '#description' => t('The client will upload bigger files in portions. Must be lower then server\'s max POST size.'),
      '#default_value' => variable_get('commerceml_file_limit', 2000000),
  );
  // CommerceML catalog import zip support.
  $form['commerceml_zip_support'] = array(
      '#type' => 'select',
      '#title' => t('ZIP support'),
      '#description' => t('Enable to compress uploads. The web server must support "unzip" command execution via exec().'),
      '#options' => array('no' => t('No'), 'yes' => t('Yes')),
      '#default_value' => variable_get('commerceml_zip_support', 'no'),
  );
  // Path to store CommerceML import files.
  $form['commerceml_file_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to temporary store imported CommerceML files'),
      '#description' => t('Have to be in public:// as Feeds do not support other variants yet. See http://drupal.org/node/1147734'),
      '#default_value' => variable_get('commerceml_file_path', 'public://commerceml/'),
  );

  // Import options.
  $form['commerceml_catalog'] = array(
      '#type' => 'fieldset',
      '#title' => t('Catalog import'),
  );
  // Feed importer settings.
  $options = array('' => t('None'));
  if ($importers = feeds_importer_load_all()) {
    foreach ($importers as $importer) {
      if ($importer->disabled ||
      !(user_access('import ' . $importer->id . ' feeds') || user_access('administer feeds')) ||
      !empty($importer->config['content_type'])) {
        continue;
      }
      $options[$importer->id] = check_plain($importer->config['name']);
    }
  }
  if (count($options) == 1) {
    drupal_set_message(t('There are no importers, go to <a href="@importers">Feed importers</a> to create one or enable an existing one.', array('@importers' => url('admin/structure/feeds'))));
  }
  $form['commerceml_catalog']['commerceml_feed_catalog'] = array(
      '#type' => 'select',
      '#title' => t('Catalog taxonomy Feeds importer'),
      '#description' => t('This Feed would be executed at once on the first import stage. It usually works with import.xml file.'),
      '#default_value' => variable_get('commerceml_feed_catalog', ''),
      '#options' => $options,
  );
  $commerceml_feed_properties_options = $options;
  array_shift($commerceml_feed_properties_options);
  $form['commerceml_catalog']['commerceml_feed_products'] = array(
      '#type' => 'select',
      '#title' => t('Product entities Feeds importer'),
      '#description' => t('This Feed would be executed progressively on the third import stage. It usually works with offers.xml file. You can override importer machine name with "feed_products" GET parameter.'),
      '#default_value' => variable_get('commerceml_feed_products', ''),
      '#options' => $options,
  );
  $form['commerceml_catalog']['commerceml_feed_product_reference'] = array(
      '#type' => 'select',
      '#title' => t('Product reference nodes Feeds importer'),
      '#description' => t('This Feed would be executed progressively on the final import stage. It usually works with import.xml file. Disable it if you want to import prices and stock only.'),
      '#default_value' => variable_get('commerceml_feed_product_reference', ''),
      '#options' => $options,
  );
  $form['commerceml_catalog']['commerceml_feed_product_reference_no_images'] = array(
      '#type' => 'select',
      '#title' => t('Product reference nodes without images Feeds importer'),
      '#description' => t('Same as above but should not import or delete product node images. Used instead of above in exchange without images. Disable it if you want to import prices and stock only.'),
      '#default_value' => variable_get('commerceml_feed_product_reference_no_images', ''),
      '#options' => $options,
  );
  $form['commerceml_catalog']['commerceml_feed_unpublish_non_existing_nodes'] = array(
      '#type' => 'checkbox',
      '#title' => t('Unpublish non-existing product nodes during full catalog exchange'),
      '#description' => t('Override configured Feeds action to take when previously imported nodes are missing in the feed. If checked, missing nodes will be unpublished during full catalog exchange. Requires Feeds dev-version to work!'),
      '#default_value' => variable_get('commerceml_feed_unpublish_non_existing_nodes', TRUE),
  );
  /*$form['commerceml_catalog']['commerceml_feed_properties_create'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create product node properties'),
      '#description' => t('Create new fields for product node type before import. New taxonomy vocabulary, product node taxonomy reference field and vocabulary fields importer are created for list properties. Product node string field is created for string properties. Product node importers (both with images and not) are modified to import new fields.'),
      '#default_value' => variable_get('commerceml_feed_properties_create', TRUE),
  );
  // Product node type.
  $options = array();
  foreach (node_type_get_types() as $type) {
    $options[$type->type] = $type->name;
  }
  $form['commerceml_catalog']['commerceml_product_node_type'] = array(
      '#type' => 'select',
      '#title' => t('Product node type'),
      '#description' => t('New fields will be added to this node type.'),
      '#default_value' => variable_get('commerceml_product_node_type', 'product_display'),
      '#options' => $options,
  );*/

  $form['commerceml_catalog']['commerceml_feeds_copy_from'] = array(
    '#type' => 'textfield',
    '#title' => 'Feed copy from',
    '#default_value' => variable_get('commerceml_feeds_copy_from', 'd7c_commerceml_feeds_products'),
  );
  $form['commerceml_catalog']['commerceml_product_copy_from'] = array(
    '#type' => 'textfield',
    '#title' => 'Product copy from',
    '#default_value' => variable_get('commerceml_product_copy_from', 'product'),
  );
  
  // Export options.
  $form['commerceml_sale'] = array(
      '#type' => 'fieldset',
      '#title' => t('Order exchange'),
  );
  $form['commerceml_sale']['commerceml_order_number_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Order number prefix'),
      '#description' => t('Prefix for order number export. Leave empty for none.'),
      '#default_value' => variable_get('commerceml_order_number_prefix', ''),
  );
  $form['commerceml_sale']['commerceml_order_number_pad'] = array(
      '#type' => 'textfield',
      '#title' => t('Order number left pad'),
      '#description' => t('Left pad order number with zeroes to specified length excluding prefix. Leave empty for none.'),
      '#default_value' => variable_get('commerceml_order_number_pad', ''),
  );
  $form['commerceml_sale']['commerceml_order_shipping'] = array(
      '#type' => 'select',
      '#title' => t('Export shipping'),
      '#description' => t('Export delivery line items.'),
      '#options' => array(0 => t('No'), 1 => t('Yes')),
      '#default_value' => variable_get('commerceml_order_shipping', 1),
  );
  // Order export status.
  $options = array('' => t('none'));
  foreach (commerce_order_statuses() as $name => $status) {
    $options[$name] = sprintf('[%s] %s', $name, $status['title']);
  }
  $form['commerceml_sale']['commerceml_order_export_status'] = array(
      '#type' => 'select',
      '#title' => t('Order status for exporting'),
      '#description' => t('The orders in this status will be exported during CommerceML sale exchange. Choosing none will disable order export.'),
      '#default_value' => variable_get('commerceml_order_export_status', 'pending'),
      '#options' => $options,
  );
  $form['commerceml_sale']['commerceml_order_update_status'] = array(
      '#type' => 'select',
      '#title' => t('Order export update status'),
      '#description' => t('The orders will change status upon successful export. Choosing none will disable order export status update.'),
      '#default_value' => variable_get('commerceml_order_update_status', 'processing'),
      '#options' => $options,
  );
  $form['commerceml_sale']['commerceml_order_payment_status'] = array(
      '#type' => 'select',
      '#title' => t('Order exchange payment status'),
      '#description' => t('The orders will change status upon reciving payment details.'),
      '#default_value' => variable_get('commerceml_order_payment_status', ''),
      '#options' => $options,
  );
  $form['commerceml_sale']['commerceml_order_shipment_status'] = array(
      '#type' => 'select',
      '#title' => t('Order exchange shipment status'),
      '#description' => t('The orders will change status upon reciving shipment details.'),
      '#default_value' => variable_get('commerceml_order_shipment_status', 'completed'),
      '#options' => $options,
  );
  // Order type token.
  $form['commerceml_sale']['commerceml_token_order_type'] = array(
      '#type' => 'textfield',
      '#title' => t('Order payment type'),
      '#description' => t('A field to determine if order is for person or organization. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_order_type', ''),
  );
  // Order type organization value.
  $form['commerceml_sale']['commerceml_token_order_type_org'] = array(
      '#type' => 'textfield',
      '#title' => t('Order type value for organizations'),
      '#description' => t('A value for previous field for organization orders.'),
      '#default_value' => variable_get('commerceml_token_order_type_org', 'Юридическое лицо'),
  );
  // Name token.
  $form['commerceml_sale']['commerceml_token_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Customer name. Used as full person name in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_name', ''),
  );
  // Organization token.
  $form['commerceml_sale']['commerceml_token_org_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Organization'),
      '#description' => t('Organization name. Used as organization name in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_org_name', ''),
  );
  // Order comment.
  $form['commerceml_sale']['commerceml_token_comment'] = array(
      '#type' => 'textarea',
      '#title' => t('Order comment'),
      '#description' => t('Order additional information. Should contain text and any tokens from below.'),
      '#default_value' => variable_get('commerceml_token_comment', ''),
  );
  // Mail token.
  $form['commerceml_sale']['commerceml_token_mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#description' => t('Customer email. Used as mail in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_mail', ''),
  );
  // Phone token.
  $form['commerceml_sale']['commerceml_token_phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#description' => t('Customer phone. Used as phone in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_phone', ''),
  );
  // INN token.
  $form['commerceml_sale']['commerceml_token_inn'] = array(
      '#type' => 'textfield',
      '#title' => t('INN'),
      '#description' => t('Customer INN. Used as INN in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_inn', ''),
  );
  // KPP token.
  $form['commerceml_sale']['commerceml_token_kpp'] = array(
      '#type' => 'textfield',
      '#title' => t('KPP'),
      '#description' => t('Customer KPP. Used as KPP in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_kpp', ''),
  );
  // Legal address token.
  $form['commerceml_sale']['commerceml_token_legal_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Legal Address'),
      '#description' => t('Customer legal address. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_legal_address', ''),
  );
  // Bank account number token.
  $form['commerceml_sale']['commerceml_token_bank_account_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank account number'),
      '#description' => t('Bank account number. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_bank_account_number', ''),
  );
  // Bank bik number token.
  $form['commerceml_sale']['commerceml_token_bank_bic'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank BIC'),
      '#description' => t('Bank BIC. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_bank_bic', ''),
  );
  // Address token.
  $form['commerceml_sale']['commerceml_token_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#description' => t('Customer address. Used as full address in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_address', ''),
  );
  // ZIP token.
  $form['commerceml_sale']['commerceml_token_zip'] = array(
      '#type' => 'textfield',
      '#title' => t('ZIP code'),
      '#description' => t('Customer zip code. Used as area code in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_zip', ''),
  );
  // Country token.
  $form['commerceml_sale']['commerceml_token_country'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#description' => t('Customer Country. Used as country in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_country', ''),
  );
  // City token.
  $form['commerceml_sale']['commerceml_token_city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#description' => t('Customer locality. Used as city in CommerceML. Should contain a token from below.'),
      '#default_value' => variable_get('commerceml_token_city', ''),
  );
  // Shipping address.
  $form['commerceml_sale']['commerceml_token_shipping_address'] = array(
      '#type' => 'textarea',
      '#title' => t('Shipping address'),
      '#description' => t('Order shipping address. Should contain text and any tokens from below.'),
      '#default_value' => variable_get('commerceml_token_shipping_address', ''),
  );
  // Token helper table.
  $form['commerceml_sale']['token_tree'] = array(
      '#theme' => 'token_tree',
      // Specific token types to include.
      //'#token_types' => array('commerce-customer-profile', 'commerce-order', 'user'),
      '#token_types' => array('commerce-order'),
      // Whether or not to include global token types like current-user, date, etc.
      '#global_types' => FALSE,
      // Make tokens clickable & insert into last focused textfield.
      '#click_insert' => TRUE,
      // Since tokens like comment and taxonomy terms can recurse infinitely, we have to set some kind of limit.
      '#recursion_limit' => 2,
  );

  return system_settings_form($form);
}

/**
 * Form callback: sync structure form.
 */
function commerceml_sync_form($form, &$form_state) {
  $form = array();
  $form['entity_type'] = array(
    '#type' => 'textfield',
    '#title' => 'Entity type',
    '#default_value' => 'commerce_product',
  );
  $form['bundle_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Bundle name',
    '#default_value' => 'product_new',
  );
  $form['feed_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Feed name',
    '#default_value' => 'd7c_commerceml_feeds_products_new',
  );
  $form['commerceml_feeds_copy_from'] = array(
    '#type' => 'textfield',
    '#title' => 'Feed copy from',
    '#default_value' => variable_get('commerceml_feeds_copy_from', 'd7c_commerceml_feeds_products'),
  );
  $form['commerceml_product_copy_from'] = array(
    '#type' => 'textfield',
    '#title' => 'Product copy from',
    '#default_value' => variable_get('commerceml_product_copy_from', 'product'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Syncronize fields',
  );
  return $form;
}

/**
 * Form submit: sync structure form.
 */
function commerceml_sync_form_submit($form, &$form_state) {
  variable_set('commerceml_feeds_copy_from', $form_state['values']['commerceml_feeds_copy_from']);
  variable_set('commerceml_product_copy_from', $form_state['values']['commerceml_product_copy_from']);
  $commerceml_file_path = variable_get('commerceml_file_path', 'public://commerceml/');
  $commerceml_file_name = $commerceml_file_path . 'import0_1.xml';
  $xml = simplexml_load_file($commerceml_file_name);
  $import = commerceml_import_parse($xml);
  $options = array(
    // Current module name.
    'module_name' => 'commerceml',
    // Field machine name prefix used when creating new fields or searching for fields to delete/update.
    // We use empty prefix here to use guid for field name and do not exceed 32 charachers limit.
    'field_name_prefix' => 'c_',
    // Entity type to syncronize fields for, usually node or commerce_product.
    'entity_type' => $form_state['values']['entity_type'],
    // Entity bundle of given type to syncronize fields for.
    'bundle_name' => $form_state['values']['bundle_name'],
    // Feed importer machine name to syncronize fields.
    'feed_name' => $form_state['values']['feed_name'],
  );
  $batch = commerceml_batch($import, $options);
  batch_set($batch);
}
