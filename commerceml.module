<?php

// Import states between progress calls.
define('COMMERCEML_IMPORT_STATE_INIT', 0);
define('COMMERCEML_IMPORT_STATE_ARG', 1);
define('COMMERCEML_IMPORT_STATE_CATALOG', 2);
define('COMMERCEML_IMPORT_STATE_PROPERTIES', 3);
define('COMMERCEML_IMPORT_STATE_PRODUCTS', 4);
define('COMMERCEML_IMPORT_STATE_PRODUCT_NODES', 5);
define('COMMERCEML_IMPORT_STATE_DONE', 6);

// Number of seconds for batch import to send progress message
// and stop importing till the next progress call.
// Should be less then server PHP timeout.
// Decrease to reduce load if server stops responding on a weak host.
define('COMMERCEML_IMPORT_PROGRESS_TIMEOUT', 20);

// Create field names with prefix. Do not leave empty.
define('COMMERCEML_FIELD_NAME_PREFIX', 'c_');

/**
 * Implements hook_menu().
 */
function commerceml_menu() {
  // Emulate Bitrix path for moysklad.ru etc.
  $items['bitrix/admin/1c_exchange.php'] = array(
    'page callback' => 'commerceml_exchange',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  // Nice path for manual configuration.
  $items['commerceml_exchange'] = array(
    'page callback' => 'commerceml_exchange',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['admin/commerce/config/commerceml'] = array(
    'title' => 'CommerceML settings',
    'description' => 'Configure CommerceML exchange.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerceml_settings_form'),
    'access arguments' => array('configure commerceml'),
    'file' => 'commerceml.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/commerce/config/commerceml/settings'] = array(
    'title' => 'CommerceML settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/commerce/config/commerceml/sync'] = array(
    'title' => 'Syncronize Structure',
    'description' => 'Parse CommerceML and syncronize fields for given product type.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerceml_sync_form'),
    'access arguments' => array('configure commerceml'),
    'file' => 'commerceml.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function commerceml_theme() {
  return array (
    'commerceml_orders' => array(
      'template' => 'commerceml_orders',
      'arguments' => array('orders' => array()),
    ),
  );
}

/**
 * Implements hook_permission().
 */
function commerceml_permission() {
  return array(
    'configure commerceml' => array(
      'title' => t('Configure CommerceML exchange'),
      'description' => t('Allows users to configure CommerceML exchange settings for the store.'),
      'restrict access' => TRUE,
    ),
    'exchange commerceml' => array(
      'title' => t('Perform CommerceML exchange'),
      'description' => t('Allows users to access CommerceML exchange web service to import products and export orders.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Menu callback. Implements http://v8.1c.ru/edi/edi_stnd/131/
 */
function commerceml_exchange() {
  @set_time_limit(0);

  watchdog('commerceml', 'CommerceML exchange initiated:<br> <pre>@server</pre>', array('@server' => print_r($_SERVER, TRUE)));

  // Check for invalid parameters.
  if (empty($_GET['type']) || empty($_GET['mode'])) {
    watchdog('commerceml', 'Empty command type or mode.', array(), WATCHDOG_WARNING);
    _commerceml_failure('Empty command type or mode.');
  }

  global $user;

  // Fix HTTP auth for CGI PHP mode.
  if (empty($_SERVER['PHP_AUTH_USER']) && (!empty($_SERVER['REMOTE_USER']) || !empty($_SERVER['REDIRECT_REMOTE_USER']))) {
    $remote_user = !empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] : $_SERVER['REDIRECT_REMOTE_USER'];
    // Cut "Basic " and and decode user:pw from Authenticate request header.
    $user_pw = base64_decode(substr($remote_user, 6));
    if ($user_pw) {
      list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', $user_pw);
    }
  }

  // Authorization.
  if ($_GET['mode'] == 'checkauth' && !empty($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_PW'])) {
    // Check HTTP authentication to set session cookies... WTF!
    $uid = user_authenticate($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
    if ($uid > 0 && ($account = user_load($uid)) && user_access('exchange commerceml', $account)) {
      // Replace global user.
      $user = $account;
      // Handle session.
      drupal_session_regenerate();
      watchdog('commerceml', 'Successful login for @login.', array('@login' => $_SERVER['PHP_AUTH_USER']));
      // Pass session to client.
      _commerceml_success(session_name() . "\n" . session_id() . "\n");
    }
    else {
      watchdog('commerceml', 'Access denied for @login.',
        array('@login' => $_SERVER['PHP_AUTH_USER']), WATCHDOG_WARNING);
      _commerceml_failure('Access denied.');
    }
  }

  // Authentication.
  if (!user_access('exchange commerceml')) {
    watchdog('commerceml', 'Unauthorized.', array(), WATCHDOG_WARNING);
    _commerceml_failure('Unauthorized.');
  }

  // Handle the type/mode command.
  switch ($_GET['type']) {
  case 'sale':
    // Export orders.
    _commerceml_exchange_sale();
    break;
  case 'catalog':
    // Import products.
    _commerceml_exchange_catalog();
    break;
  default:
    watchdog('commerceml', 'Unknown command type: @type.', array('@type' => $_GET['type']), WATCHDOG_WARNING);
    _commerceml_failure('Unknown command type.');
  }
}

/**
 * Handles CommerceML sale exchange type.
 */
function _commerceml_exchange_sale() {
  switch ($_GET['mode']) {
  case 'init':
    // Initialize exchange.
    watchdog('commerceml', 'Sale exchange initialized.');
    $zip = variable_get('commerceml_zip_support', 'no');
    // TODO: switch to yes after order update implementation.
    print("zip=no\n");
    print('file_limit=' . variable_get('commerceml_file_limit', 2000000));
    break;

  case 'query':
    // Send new orders.
    $orders = array();
    $status = variable_get('commerceml_order_export_status', 'pending');
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_order')
      ->propertyCondition('status', $status);
    // Filter orders from GET-parameter field_name[column]=value.
    // For example field_supplier[target_id]=7.
    $filter = arg(1);
    $matches = array();
    if (!empty($filter) && preg_match('/^([a-z0-9_]+)\[([a-z0-9_]+)\]=([a-z0-9_A-Z]+)$/', $filter, $matches)) {
      $query->fieldCondition($matches[1], $matches[2], $matches[3]);
    }
    $order_ids = $query->execute();
    if (!empty($order_ids['commerce_order']) && !empty($status)) {
      $order_ids = array_keys($order_ids['commerce_order']);
      $orders = commerce_order_load_multiple($order_ids, array(), TRUE);
    }
    if (count($orders) > 0) {
      foreach ($orders as $oid => &$order) {
        // Lookup line items.
        $li_ids = array();
        foreach ($order->commerce_line_items['und'] as $li) {
          $li_ids[] = $li['line_item_id'];
        }
        $lis = commerce_line_item_load_multiple($li_ids);

        // Filter delivery.
        $commerceml_order_shipping = variable_get('commerceml_order_shipping', 1);
        if (empty($commerceml_order_shipping)) {
          foreach ($lis as $lid => $li) {
            if ($li->type == 'shipping') {
              unset($lis[$lid]);
            }
          }
        }

        // Lookup products.
        $product_ids = array();
        foreach ($lis as $li) {
          if (!empty($li->commerce_product['und'])) {
            foreach ($li->commerce_product['und'] as $prod_ref) {
              $product_ids[] = $prod_ref['product_id'];
            }
          }
        }
        $products = commerce_product_load_multiple($product_ids);

        // Store additional data to orders for template output.
        $order->commerceml_user = user_load($order->uid);
        if (empty($order->uid)) {
          $order->commerceml_user_id = '0#' . (empty($order->mail) ? 'unregistered' : $order->mail) . '#';
        }
        else {
          $order->commerceml_user_id = $order->commerceml_user->uid . '#' . $order->commerceml_user->name .'#';
        }

        $order->commerceml_line_items = $lis;
        foreach ($lis as $li) {
          if (!empty($li->commerce_product['und'])) {
            $product_id = $li->commerce_product['und'][0]['product_id'];

            // Get guid from feed importer if we prevoiusly imported products.
            $guid = db_select('feeds_item')
              ->fields('feeds_item', array('guid'))
              ->condition('entity_type', 'commerce_product')
              ->condition('entity_id', $product_id)
              ->execute()
              ->fetchField();
            $products[$product_id]->commerceml_guid = $guid ? $guid : $product_id;
          }
        }
        $order->commerceml_products = $products;

        // Add status.
        foreach (commerce_order_statuses() as $id => $status) {
          if ($id == $order->status) {
            $order->commerceml_status = sprintf('[%s] %s', $id, $status['title']);
            $order->commerceml_status_title = $status['title'];
            break;
          }
        }

        // Pad order number.
        $commerceml_order_number_pad = variable_get('commerceml_order_number_pad', '');
        if (!empty($commerceml_order_number_pad) && is_numeric($commerceml_order_number_pad)) {
          $order->order_number = str_pad($order->order_number, $commerceml_order_number_pad, "0", STR_PAD_LEFT);
        }

        // Prefix order number.
        $commerceml_order_number_prefix = variable_get('commerceml_order_number_prefix', '');
        if (!empty($commerceml_order_number_prefix)) {
          $order->order_number = $commerceml_order_number_prefix . $order->order_number;
        }
      }
    }

    // Get response XML from template.
    $xml = theme('commerceml_orders', array('orders' => $orders));
    header('Content-Type: application/xml');
    // 1C does not parse UTF-8 XML for some reason, use CP1251.
    print(iconv('utf-8', 'windows-1251', $xml));

    watchdog('commerceml', 'Sale query with @count new orders: @orders',
      array('@count' => count($orders), '@orders' => implode(', ', array_keys($orders))));
    break;

  case 'success':
    // Change exported orders status.
    $status_export = variable_get('commerceml_order_export_status', 'pending');
    $status_update = variable_get('commerceml_order_update_status', 'processing');
    if (!empty($status_export) && !empty($status_update)) {
      $orders = commerce_order_load_multiple(array(), array('status' => $status_export), TRUE);
      foreach ($orders as $order) {
        commerce_order_status_update($order, $status_update, FALSE, NULL, t('Order exported to CommerceML.'));
      }
      watchdog('commerceml', 'Sale success with @count updated orders: @orders',
        array('@count' => count($orders), '@orders' => implode(', ', array_keys($orders))));
    }
    _commerceml_success();
    break;

  case 'file':
    // Update orders.
    $payment_status = variable_get('commerceml_order_payment_status', '');
    $shipment_status = variable_get('commerceml_order_shipment_status', 'completed');
    // Only status update implemented yet.
    if ($payment_status || $shipment_status) {
      // Save (overwrite) uploaded file and open orders XML.
      _commerceml_upload_file('w');
      $path = variable_get('commerceml_file_path', 'public://commerceml/');
      $filename = $path . $_GET['filename'];
      $xml = @simplexml_load_file($filename);
      if (!$xml) {
        watchdog('commerceml', 'Failed to load file @file.',
          array('@file' => $_GET['filename']), WATCHDOG_WARNING);
        _commerceml_failure('Failed to load file ' . $_GET['filename']);
      }

      // Loop through all documents and change order statuses if needed.
      foreach ($xml->children() as $doc) {
        $order_id = (string)$doc->Номер;
        // Try to cut prefix if any.
        while (!is_numeric($order_id) && strlen($order_id) > 0) {
          $order_id = substr($order_id, 1);
        }
        $order_id = intval($order_id);
        if (empty($order_id) || !is_numeric($order_id)) {
          watchdog('commerceml', 'Skipped order with number @order_number',
            array('@order_number' => (string)$doc->Номер), WATCHDOG_WARNING);
          continue;
        }
        $order = commerce_order_load($order_id);
        if ($order) {
          $status_update = '';
          foreach ($doc->ЗначенияРеквизитов->ЗначениеРеквизита as $node) {
            if ($node->Наименование == 'Дата оплаты по 1С' &&
                strlen($node->Значение) > 0 && $payment_status) {
              $status_update = $payment_status;
              $status_comment = $node->Наименование . ' ' . $node->Значение;
            }
            if ($node->Наименование == 'Дата отгрузки по 1С' &&
                strlen($node->Значение) > 0 && $shipment_status) {
              $status_update = $shipment_status;
              $status_comment = $node->Наименование . ' ' . $node->Значение;
            }
          }
          if ($status_update) {
            commerce_order_status_update($order, $status_update, FALSE, NULL, $status_comment);
          }
        }
      }
    }
    watchdog('commerceml', 'Order update. Filename @filename, XML: <br> @xml',
      array('@filename' => $_GET['filename'], '@xml' => file_get_contents($filename)));
    _commerceml_success();
    break;

  default:
    watchdog('commerceml', 'Sale exchange unknown mode: @mode.',
      array('@mode' => $_GET['mode']), WATCHDOG_WARNING);
    _commerceml_failure('Unknown mode.');
  }
}

/**
 * Handles CommerceML sale exchange catalog.
 */
function _commerceml_exchange_catalog() {
  switch ($_GET['mode']) {
  case 'init':
    // Initialize exchange.
    unset($_SESSION['commerceml_import_state']);

    // Clear import directory if we have successful import before.
    $clear_import_directory = variable_get('commerceml_need_clear_import_directory', FALSE);
    if ($clear_import_directory) {
      _commerceml_clear_import_directory();
      variable_set('commerceml_need_clear_import_directory', FALSE);
    }

    // Respond with zip and file limit parameters.
    $zip = variable_get('commerceml_zip_support', 'no');
    print("zip=$zip\n");
    print('file_limit=' . variable_get('commerceml_file_limit', 2000000));
    break;

  case 'file':
    // Save (append) uploaded file.
    $size = _commerceml_upload_file('ab');
    watchdog('commerceml', 'Catalog file transfer success: @filename (@size bytes).',
      array('@filename' => $_GET['filename'], '@size' => $size));
    _commerceml_success();
    break;

  case 'import':
    if (isset($_SESSION['commerceml_import_state']) && $_SESSION['commerceml_import_state'] == COMMERCEML_IMPORT_STATE_DONE) {
      // Set catalog exchange successful finish flag to clear files on next init.
      variable_set('commerceml_need_clear_import_directory', TRUE);
    }
    else {
      // We wait for all files to upload and
      // do all the import work during filename=offers0_1.xml or offers.xml.
      if (empty($_GET['filename']) || $_GET['filename'] != 'offers0_1.xml' && $_GET['filename'] != 'offers.xml') {
        watchdog('commerceml', 'File @filename import skipped.', array('filename' => $_GET['filename']));
        _commerceml_success();
      }
    }

    // Import files using feeds.
    commerceml_exchange_catalog_import();
    break;

  default:
    watchdog('commerceml', 'Catalog exchange unknown mode: @mode.', array('@mode' => $_GET['mode']), WATCHDOG_WARNING);
    _commerceml_failure('Unknown mode.');
  }
}

/**
 * Handles CommerceML sale exchange catalog import via feeds in settings.
 */
function commerceml_exchange_catalog_import() {
  // Initialize import state.
  $state = !isset($_SESSION['commerceml_import_state']) ? COMMERCEML_IMPORT_STATE_INIT : $_SESSION['commerceml_import_state'];

  // Import directory.
  $commerceml_file_path = variable_get('commerceml_file_path', 'public://commerceml/');

  // Import file name.
  // As we wait for all files to upload we need to manually switch filename for Feeds.
  if ($state == COMMERCEML_IMPORT_STATE_PRODUCTS) {
    // Use new or old naming scheme.
    $commerceml_file_name = $commerceml_file_path .
      ($_GET['filename'] == 'offers0_1.xml' ? 'offers0_1.xml' : 'offers.xml');
  }
  else {
    // Use new or old naming scheme.
    $commerceml_file_name = $commerceml_file_path .
      ($_GET['filename'] == 'offers0_1.xml' ? 'import0_1.xml' : 'import.xml');
  }

  // Advance Feeds import batches from appropriate state.
  try {
    switch ($state) {
      case COMMERCEML_IMPORT_STATE_INIT:
        // Check images are uploaded.
        $dirname = dirname($commerceml_file_path . 'import_files');
        $_SESSION['commerceml_import_images'] = is_dir($dirname);

        // Get schema version.
        $xml = simplexml_load_file($commerceml_file_name);
        $_SESSION['commerceml_schema_version'] = (string)$xml['ВерсияСхемы'];

        // Override Feeds setting to unpublish previously imported product nodes on full catalog exchange.
        $unpublish_non_existing_nodes = variable_get('commerceml_feed_unpublish_non_existing_nodes', TRUE);
        if ($unpublish_non_existing_nodes) {
          $has_only_changes = (string) $xml->Каталог['СодержитТолькоИзменения'];
          $has_only_changes = $has_only_changes == 'true';
          $_SESSION['commerceml_has_only_changes'] = $has_only_changes;
        }

        // Prepare taxonomy, feeds and fields syncronization batch.
        $import = commerceml_import_parse($xml);
        $options = array(
          // Current module name.
          'module_name' => 'commerceml',
          // Field machine name prefix used when creating new fields or searching for fields to delete/update.
          'field_name_prefix' => COMMERCEML_FIELD_NAME_PREFIX,
          // Entity type to syncronize fields for, usually node or commerce_product.
          'entity_type' => 'commerce_product',
          // Entity bundle of given type to syncronize fields for.
          'bundle_name' => 'product',
          // Feed importer machine name to syncronize fields.
          'feed_name' => 'd7c_commerceml_feeds_products',
        );
        
        // Override feed name from url.
        $feed_name = arg(1);
        if (!empty($feed_name)) {
          $options['feed_name'] = $feed_name;
        }
        
        // Override bundle name from url.
        $bundle_name = arg(2);
        if (!empty($bundle_name)) {
          $options['bundle_name'] = $bundle_name;
        }
        
        $batch = commerceml_batch($import, $options);
        if (empty($batch)) {
          watchdog('commerceml', 'Failed to prepare import batch.',
            array(), WATCHDOG_WARNING);
          _commerceml_failure('Failed to prepare import batch.');
        }
        
        // TODO: Run batch progressively.
        batch_set($batch);
        $batch = &batch_get();
        $process_info = array(
          'current_set' => 0, 
          'progressive' => FALSE, 
          'url' => '', 
          'url_options' => array(), 
          'source_url' => $_GET['q'], 
          'redirect' => NULL, 
          'theme' => $GLOBALS['theme_key'], 
          'redirect_callback' => NULL,
        );
        $batch += $process_info;
        $batch['id'] = db_next_id();
        // Move operations to a job queue. Non-progressive batches will use a
        // memory-based queue.
        foreach ($batch['sets'] as $key => $batch_set) {
          _batch_populate_queue($batch, $key);
        }
        require_once DRUPAL_ROOT . '/includes/batch.inc';
        _batch_process();
        
        // Run feed from argument or proceed next to catalog import step.
        $_SESSION['commerceml_import_state'] = empty($feed_name) ? COMMERCEML_IMPORT_STATE_CATALOG : COMMERCEML_IMPORT_STATE_ARG;
        watchdog('commerceml', 'Catalog import init.');
        _commerceml_progress('Catalog init complete.');
        break;

        case COMMERCEML_IMPORT_STATE_ARG:      
          // Get feed name from URL.
          $feed = arg(1);
          if (!empty($feed)) {
            if (!feeds_importer_load($feed)) {
              watchdog('commerceml', 'Can not load feeds importer @feed.',
                array('@feed' => $feed), WATCHDOG_WARNING);
              break;
            }
          
            $feeds_source = feeds_source($feed, 0);
            $config = array('FeedsFileFetcher' => array('source' => $commerceml_file_name));
            $feeds_source->addConfig($config);

            $import_start = time();
            $progress = 0;
            // Do import until timeout or finished.
            while ($progress != FEEDS_BATCH_COMPLETE &&
                  time() - $import_start < COMMERCEML_IMPORT_PROGRESS_TIMEOUT) {
              $progress = $feeds_source->import();
            }
            watchdog('commerceml', 'Catalog argument import @persent% done.', array('@persent' => $progress * 100));
            // Send progress responce on timeout if not complete.
            if ($progress != FEEDS_BATCH_COMPLETE) {
              _commerceml_progress(sprintf('Catalog argument import %d%% done.', $progress * 100));
            }
            watchdog('commerceml', 'Argument feed import complete.');
          }
          else {
            watchdog('commerceml', 'Catalog products import skipped as no product import feed is specified.');
          }
          $_SESSION['commerceml_import_state'] = COMMERCEML_IMPORT_STATE_CATALOG;
          _commerceml_progress('Argument feed import complete.');
          break;

      case COMMERCEML_IMPORT_STATE_CATALOG:
        // Import catalog taxonomy.
        $feed = variable_get('commerceml_feed_catalog', '');
        if (!empty($feed)) {
          $feeds_source = feeds_source($feed, 0);
          $config = array('FeedsFileFetcher' => array('source' => $commerceml_file_name));
          $feeds_source->addConfig($config);
          while (FEEDS_BATCH_COMPLETE != $feeds_source->import());
          watchdog('commerceml', 'Catalog taxonomy import complete.');
        }
        else {
          watchdog('commerceml', 'Catalog taxonomy import skipped.');
        }
        $_SESSION['commerceml_import_state'] = COMMERCEML_IMPORT_STATE_PROPERTIES;
        _commerceml_progress('Catalog taxonomy import complete.');
        break;

      case COMMERCEML_IMPORT_STATE_PROPERTIES:
        // Import properties taxonomy.
        $feeds = variable_get('commerceml_feed_properties', array());
        foreach ($feeds as $feed) {
          if (!empty($feed)) {
            $feeds_source = feeds_source($feed, 0);
            $config = array('FeedsFileFetcher' => array('source' => $commerceml_file_name));
            $feeds_source->addConfig($config);
            while (FEEDS_BATCH_COMPLETE != $feeds_source->import());
            watchdog('commerceml', 'Property taxonomy import complete for @feed feed.',
              array('@feed' => $feed));
          }
          else {
            watchdog('commerceml', 'Property taxonomy import skipped for @feed feed.',
              array('@feed' => $feed));
          }
        }
        $_SESSION['commerceml_import_state'] = COMMERCEML_IMPORT_STATE_PRODUCTS;
        _commerceml_progress('Properties taxonomy import complete.');
        break;

      case COMMERCEML_IMPORT_STATE_PRODUCTS:
        // Import product entities.
        $feed = variable_get('commerceml_feed_products', '');
        if (!empty($feed)) {
          if (!feeds_importer_load($feed)) {
            watchdog('commerceml', 'Can not load feeds importer @feed.',
              array('@feed' => $feed), WATCHDOG_WARNING);
            break;
          }
          
          $feeds_source = feeds_source($feed, 0);

          // Override product type from URL.
          $bundle_name = arg(2);
          if (!empty($bundle_name)) {
            $config = array('product_type' => $bundle_name, 'bundle' => $bundle_name);
            $feeds_source->importer->processor->addConfig($config);
          }

          if (!file_exists($commerceml_file_name)) {
            watchdog('commerceml', 'Import file @file could not be found, skipping products import.',
              array('@file' => $commerceml_file_name), WATCHDOG_WARNING);
            break;
          }

          $config = array('FeedsFileFetcher' => array('source' => $commerceml_file_name));
          watchdog('commerceml', 'Catalog products import @file.', array('@file' => $commerceml_file_name));
          $feeds_source->addConfig($config);
                    
          $import_start = time();
          $progress = 0;
          // Do import until timeout or finished.
          while ($progress != FEEDS_BATCH_COMPLETE &&
                time() - $import_start < COMMERCEML_IMPORT_PROGRESS_TIMEOUT) {
            $progress = $feeds_source->import();
          }
          watchdog('commerceml', 'Catalog products import @persent% done.', array('@persent' => $progress * 100));
          // Send progress responce on timeout if not complete.
          if ($progress != FEEDS_BATCH_COMPLETE) {
            _commerceml_progress(sprintf('Catalog offers import %d%% done.', $progress * 100));
          }
          watchdog('commerceml', 'Catalog products import complete.');
        }
        else {
          watchdog('commerceml', 'Catalog products import skipped as no product import feed is specified.');
        }
        $_SESSION['commerceml_import_state'] = COMMERCEML_IMPORT_STATE_PRODUCT_NODES;
        _commerceml_progress('Catalog products import complete.');
        break;

      case COMMERCEML_IMPORT_STATE_PRODUCT_NODES:
        // Import product display nodes.
        $feed_name = $_SESSION['commerceml_import_images'] ?
          'commerceml_feed_product_reference' : 'commerceml_feed_product_reference_no_images';
        $feed = variable_get($feed_name, '');
        if (!empty($feed)) {
          $feeds_source = feeds_source($feed, 0);
          $config = array('FeedsFileFetcher' => array('source' => $commerceml_file_name));
          $feeds_source->addConfig($config);

          // Skip non existing products if import contains only changes or use default configured Feeds action otherwise.
          if (isset($_SESSION['commerceml_has_only_changes'])) {
            $feeds_source->setConfigFor($feeds_source->importer->processor, array('update_non_existent' => 'skip'));
          }

          $import_start = time();
          $progress = 0;
          // Do import until timeout or finished.
          while ($progress != FEEDS_BATCH_COMPLETE &&
                time() - $import_start < COMMERCEML_IMPORT_PROGRESS_TIMEOUT) {
            $progress = $feeds_source->import();
          }
          watchdog('commerceml', 'Catalog product display nodes import @persent% done.', array('@persent' => $progress * 100));
          // Send progress responce on timeout if not complete.
          if ($progress != FEEDS_BATCH_COMPLETE) {
            _commerceml_progress(sprintf('Catalog products import %d%% done.', $progress * 100));
          }
          watchdog('commerceml', 'Catalog product display nodes import complete.');
        }
        else {
          watchdog('commerceml', 'Catalog product display nodes import skipped.');
        }

        $_SESSION['commerceml_import_state'] = COMMERCEML_IMPORT_STATE_DONE;
        _commerceml_progress(100);
        break;

      case COMMERCEML_IMPORT_STATE_DONE:
        cache_clear_all();
        watchdog('commerceml', 'Catalog import done.');
        _commerceml_success();
        break;
    }
  }
  catch (Exception $e) {
    // Handle exception from Feeds module.
    watchdog('commerceml', 'Exception on import: @msg, @trace',
      array('@msg' => $e->getMessage(), '@trace' => $e->getTraceAsString()),
      WATCHDOG_ERROR);
    _commerceml_failure('Exception on import:' . $e->getMessage());
  }
}

function commerceml_import_parse($xml) {
  $fields_used = array();
  $fields = array();
  $terms = array();
  $vocabularies = array();

  // Get all fields used in products to skip/delete unused fields.
  $products = $xml->Каталог->Товары->Товар;
  if (empty($products) || !is_object($products)) {
    return;
  }
  foreach ($products as $product) {
    $product_fields = $product->ЗначенияСвойств->ЗначенияСвойства;
    if (empty($product_fields) || !is_object($product_fields)) {
      continue;
    }
    foreach ($product_fields as $product_field) {
      $field_id = (string)$product_field->Ид;
      $fields_used[$field_id] = $field_id;
    }
  }
  
  // Get all fields.
  $properties = $xml->Классификатор->Свойства->Свойство;
  if (empty($properties) || !is_object($properties)) {
    return;
  }
  
  foreach ($properties as $property) {
    $property_id = (string)$property->Ид;
    
    // Skip unused in products.
    if (empty($fields_used[$property_id])) {
      continue;
    }

    $property_name = (string)$property->Наименование;
    $property_type = (string)$property->ТипЗначений;

    // Entity field and taxonomy vocabulary machine name.
    // Remove minuses to fit in 32 characters.
    $field_id = str_replace('-', '', $property_id);
    // Reduce to fit prefix.
    $field_id = drupal_substr($field_id, 0, 32 - strlen(COMMERCEML_FIELD_NAME_PREFIX));
    // Make first char a letter, required for field name and add prefix.
    $machine_name = COMMERCEML_FIELD_NAME_PREFIX . $field_id;

    $field = array(
      'name' => $property_name,
    );

    switch ($property_type) {
      case 'Справочник':
        $field['type'] = 'taxonomy_reference';
        $field['vocab_machine_name'] = $machine_name;
        if (empty($vocabularies[$machine_name])) {
           $vocabularies[$machine_name] = $field;
        }
        // Single values cardinality.
        $field['cardinality'] = 1;
        // Feeds source.
        $field['source'] = 'ЗначенияСвойств/ЗначенияСвойства[Ид=\'' . $property_id . '\']/Значение/text()';
        
        // Parse taxonomy terms.
        $property_terms = $property->ВариантыЗначений->Справочник;
        if (empty($property_terms) || !is_object($property_terms)) {
          break;
        }
        foreach ($property_terms as $property_term) {
           $term_uuid = (string)$property_term->ИдЗначения;
           $term_name = (string)$property_term->Значение;
           $terms[$machine_name][$term_uuid] = array(
             'name' => $term_name,
             'id' => $term_uuid,
           );
        }
        break;
      case 'Строка':
        $field['type'] = 'string';
        // Feeds source.
        $field['source'] = 'ЗначенияСвойств/ЗначенияСвойства[Ид=\'' . $property_id . '\']/Значение/text()';
        break;
    }
    
    $fields[$field_id] = $field;
  }  

  return array(
    'fields' => $fields,
    'terms' => $terms,
    'vocabularies' => $vocabularies,
  );
}

/**
 * Returns token value from order by part of token variable name.
 * Tokens are stored in module settings.
 */
function commerceml_token($order, $token_name) {
  $token = variable_get('commerceml_token_' . $token_name, '');
  $token_data = array('commerce-order' => $order);
  $result = strip_tags(token_replace($token, $token_data));
  // Remove unused tokens.
  $result = preg_replace('/\[.*\]/', '', $result);
  return $result == $token ? '' : $result;
}

/**
 * Upload file in append or rewrite mode.
 */
function _commerceml_upload_file($mode = 'ab') {
  // Validate file name.
  if (empty($_GET['filename'])) {
    watchdog('commerceml', 'Empty file name.', array(), WATCHDOG_WARNING);
    _commerceml_failure('Empty file name.');
  }
  if (!preg_match('/^[0-9a-zA-Z_\-.\/]+$/', $_GET['filename'])) {
    watchdog('commerceml', 'Incorrect file name.', array(), WATCHDOG_WARNING);
    _commerceml_failure('Incorrect file name.');
  }
  // Add path.
  $filename = variable_get('commerceml_file_path', 'public://commerceml/') .
    trim(str_replace('\\', '/', trim($_GET['filename'])), '/');
  // Check directory path, permissions, create if needed.
  $dirname = dirname($filename);
  if (strpos($filename, '../') !== FALSE ||
      !file_prepare_directory($dirname, FILE_CREATE_DIRECTORY)) {
    watchdog('commerceml', 'Failed to prepare directory for @filename.',
      array('@filename' => $filename), WATCHDOG_WARNING);
    _commerceml_failure('Failed to prepare directory.');
  }
  // Open file for append or overwrite.
  if (!($f = fopen($filename, $mode))) {
    watchdog('commerceml', 'Error opening file @filename.',
      array('@filename' => $filename), WATCHDOG_WARNING);
    _commerceml_failure('Error opening file.');
  }
  // Read data, append or overwrite, check result.
  $data = file_get_contents('php://input');
  $result = fwrite($f, $data);
  $size = strlen($data);
  if ($result !== $size) {
    watchdog('commerceml', 'Wrong data size written.', array(), WATCHDOG_WARNING);
    _commerceml_failure('Wrong data size written.');
  }
  // Try to unzip.
  // Archive may not be complete so we fail silently in case of unpack errors.
  // TODO: Move unpacking to later stage.
  if (variable_get('commerceml_zip_support', 'no') == 'yes' &&
      substr($filename, -3) == 'zip') {

    $dirname = drupal_realpath($dirname);
    $filename = drupal_realpath($filename);
    $zip_open_error = FALSE;

    // Try zip class first and exec(unzip ... ) second.
    if (class_exists('ZipArchive')) {
      $zip = new ZipArchive;
      $res = $zip->open($filename, ZIPARCHIVE::CHECKCONS);
      if ($res === TRUE) {
        if (!($zip->extractTo($dirname))) {
          watchdog('commerceml', 'Zip extract error.', array(), WATCHDOG_WARNING);
        }
        $zip->close();
      }
      else {
        $zip_open_error = TRUE;
        watchdog('commerceml', 'Zip open error code = @code, filename = @filename.',
          array('@code' => $res, '@filename' => $filename), WATCHDOG_WARNING);
      }
    }
    else {
      $zip_open_error = TRUE;
    }
    if ($zip_open_error) {
      exec("unzip -oqq $filename -d $dirname");
    }
  }
  return $size;
}

/**
 * Clear import directory.
 */
function _commerceml_clear_import_directory() {
  $path = variable_get('commerceml_file_path', 'public://commerceml/');
  if (file_exists($path)) {
    if (!file_unmanaged_delete_recursive($path)) {
      watchdog('commerceml', 'Error clearing directory @dir.', array('@dir' => $path), WATCHDOG_ERROR);
      _commerceml_failure('Error clearing import directory.');
    }
    else {
      watchdog('commerceml', 'Import directory cleared.');
    }
  }
}

/**
 * Commerceml structure import batch settings.
 */
function commerceml_batch($import, $options) {
  $module_name = $options['module_name'];
  $batch = array(
    'operations' => array(
      array($module_name . '_vocabularies_process', array($import['vocabularies'], $options)),
      array($module_name . '_terms_process', array($import['terms'], $options)),
      array($module_name . '_fields_process', array($import['fields'], $options)),
      array($module_name . '_feeds_process', array($import['fields'], $options)),
    ),
    'finished' => $module_name . '_batch_finished',
    'title' => t('Importing taxonomy and fields'),
    'init_message' => t('Import is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Import has encountered an error.'),
    'file' => drupal_get_path('module', $module_name) . '/' . $module_name . '.batch.inc',
  );
  // TODO: form_state redirect = FALSE
  // TODO: progressive = TRUE
  return $batch;
}

/**
 * Prints CommerceML success message and exits.
 */
function _commerceml_success($message = '') {
  print("success\n");
  print($message);
  drupal_exit();
}

/**
 * Prints CommerceML failure message and exits.
 */
function _commerceml_failure($message = '') {
  print("failure\n");
  print($message);
  drupal_exit();
}

/**
 * Prints CommerceML progress message and exits.
 */
function _commerceml_progress($message = '') {
  print("progress\n");
  print($message);
  drupal_exit();
}
