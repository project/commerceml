The module implements CommerceML exchange web service for Drupal Commerce.

-----------------

Модуль реализует веб-сервис обмена CommerceML для Drupal Commerce:
http://v8.1c.ru/edi/edi_stnd/131/

Модуль позволяет максимально автоматизировать синхронизацию базы интернет-магазина на Drupal Commerce и 1С Предприятия с помощью типовой встроенной в 1С обработки обмена с сайтом. Предлагаемое решение пока не готово для конечных пользователей, а подходит только для разработчиков интернет-магазинов. Требуется тонкая настройка Drupal Commerce, Feeds и модуля под обмен конкретных конфигураций 1С, магазина и бизнес-процесс заказчика.

Экспорт заказов завязан на некоторые поля заказа и профиля клиента через токены. Выгружаются все заказы в заданном в настройках статусе и после успешной выгрузки меняют состояние на заданное в настройках.

Импорт (создание и обновление) сущностей продуктов и нодов дисплеев продуктов реализуется через Commerce Feeds. Т.е. импортируется только то, что предварительно настроенно для импорта в Feeds под конкретный магазин и выгрузку (см. http://www.drupal.ru/node/79777).

Пока не реализовано:
- выгрузка некторых полей в заказах;
- изменение табличной части заказа при его изменении в 1С.

Установка:
0. Чистые ссылки (Clean URLs) необходимы для правильной работы обмена с 1С.
1. Включить модуль, настроить права доступа "Perform CommerceML exchange".
2. В 1C:Предприятие в настройках обмена с сайтом задать точку входа http://example.com/commerceml_exchange.
3. В 1C:Предприятие указать логин и пароль пользователя Drupal с правами "Perform CommerceML exchange". Могут также понадробиться права на создание/обновление нодов товаров, запуск Feeds.
4. На странице настроек /admin/commerce/config/commerceml настроить путь временного хранения файлов и выбрать используемые для импорта Feed importers. У пользователя, под которым выполняется обмен, должны быть права на выбранные Feed importers. Загрузка номенклатуры должна работать. В процессе обмена модуль пишет все запросы и ошибки в лог Drupal.
5. Для настройки выгрузки заказов надо задать токены для выбора параметров заказа и профиля пользователя на странице настройки модуля.

Импорт каталога поддерживается из двух файлов import.xml и offers.xml или import0_1.xml и offers0_1.xml и происходит за 8 шагов:
0. Инициализация. Очищается каталог, указанный в настройках импорта. Закачиваются все новые файлы пока не будет получен файл offers.
1. Синхронизация структуры. В память целиком парсится файл import, для всех свойств создаются словари и термины таксономии. Для сущности commerce_product бандла product (либо бандла, машинное имя которого указано в arg(2) URL обмена) синхронизируются поля. Если в arg(2) указан несуществующий тип товара, то он создается копированием указанного в настройках модуля типа товара со всеми его полями. Для XML Feeds импортера d7c_commerceml_feeds_products (либо импортера, машинное имя которого указано в arg(1) URL обмена) синхронизируются поля и мэппинг. Если в arg(1) указан несуществующий фид, то он создается копированием указанного в настройках фида.
2. Запуск Feed-импортера, машинное имя которого указано в arg(1) URL обмена, для импорта товаров из файла import. Если там ничего не указано, то шаг пропускается.
3. Запуск Feed-импортера, заданного в настройках модуля для импорта каталога из файла import.
4. Запуск дополнительных Feed-импортеров, указанных в настройках модуля для импорта дополнительных свойств из файла import.
5. Запуск Feed-импортера, заданного в настройках модуля для обновления цен и остатков товаров commerce из файла offers. Если в arg(2) указано машинное имя бандла товаров, то оно перезаписывает присвоенный в настройках этого Feed-импортера тип товаров.
6. Запуск Feed-импортера, заданного в настройках модуля для импорта нодов товаров из файла import.
7. Очистка кеша.

По вопросам поддержки, внедрения обмена с 1С и доработки модуля обращайтесь на info@drupal-coder.ru.

(c) Сергей Синица 2012-2017
