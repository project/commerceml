<?php
/**
 * @file
 * Feeds Default Template
 */

/**
 * Returns Feeds Importer object.
 */
function commerceml_feeds_template_property($id, $name, $guid) {
  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = $id;
  $feeds_importer->config = array(
    'name' => $name,
    'description' => t('@name taxonomy importer automatically created during CommerceML exchange.',
              array('@name' => $name)),
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml html htm',
        'direct' => 1,
        'directory' => 'public://commerceml',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
        'source' => 'public://commerceml/import.xml',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsXPathParserXML',
      'config' => array(
        'sources' => array(
          'xpathparser:0' => 'ИдЗначения/text()',
          'xpathparser:1' => 'Значение/text()',
        ),
        'rawXML' => array(
          'xpathparser:0' => 0,
          'xpathparser:1' => 0,
        ),
        'context' => '//КоммерческаяИнформация/Классификатор/Свойства/Свойство/Ид[.=\'' . $guid . '\']/following-sibling::ВариантыЗначений/Справочник',
        'exp' => array(
          'errors' => 0,
          'debug' => array(
            'context' => 0,
            'xpathparser:0' => 0,
            'xpathparser:1' => 0,
          ),
        ),
        'allow_override' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsTermProcessor',
      'config' => array(
        'vocabulary' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'xpathparser:0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'xpathparser:1',
            'target' => 'name',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => $id,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  return $feeds_importer;
}

/**
 * Returns part of Feeds Importer object config to add a field import.
 */
function commerceml_feeds_template_property_add_taxonomy($count, $guid, $field) {
  return array(
    'parser' => array(
      'config' => array(
        'sources' => array(
          'xpathparser:' . $count => 'ЗначенияСвойств/ЗначенияСвойства[Ид=\'' . $guid . '\']/Значение/text()',
        ),
        'rawXML' => array(
          'xpathparser:' . $count => 0,
        ),
        'exp' => array(
          'errors' => 0,
          'debug' => array(
            'xpathparser:' . $count => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'config' => array(
        'mappings' => array(
          $count => array(
            'source' => 'xpathparser:' . $count,
            'target' => $field,
            'term_search' => 2,
            'autocreate' => 0,
          ),
        ),
      ),
    ),
  );
}

/**
 * Returns part of Feeds Importer object config to add a field import.
 */
function commerceml_feeds_template_property_add_text($count, $guid, $field) {
  return array(
      'parser' => array(
          'config' => array(
              'sources' => array(
                  'xpathparser:' . $count => 'ЗначенияСвойств/ЗначенияСвойства[Ид=\'' . $guid . '\']/Значение/text()',
              ),
              'rawXML' => array(
                  'xpathparser:' . $count => 0,
              ),
              'exp' => array(
                  'errors' => 0,
                  'debug' => array(
                      'xpathparser:' . $count => 0,
                  ),
              ),
          ),
      ),
      'processor' => array(
          'config' => array(
              'mappings' => array(
                  $count => array(
                      'source' => 'xpathparser:' . $count,
                      'target' => $field,
                      'unique' => FALSE,
                  ),
              ),
          ),
      ),
  );
}

