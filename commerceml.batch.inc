<?php

/**
 * Batch operation to update Feeds configuration.
 */
function commerceml_feeds_process($fields, $options, &$context) {
  $module_name = $options['module_name'];
  $field_name_prefix = $options['field_name_prefix'];
  $entity_type = $options['entity_type'];
  $bundle_name = $options['bundle_name'];
  $feed_name = $options['feed_name'];
  $importer = feeds_importer_load($feed_name);
  
  $fields_existing = field_info_instances($entity_type, $bundle_name);
  $tamper_plugins = feeds_tamper_load_by_importer($feed_name, FALSE);
  $tamper_plugins_new = array();

  // Create new feed importer via cloning existing one.
  if (!$importer) {
    $importer_copy_from_name = variable_get('commerceml_feeds_copy_from', 'd7c_commerceml_feeds_products');
    $importer_copy_from = feeds_importer_load($importer_copy_from_name);
    if (!$importer_copy_from) {
      // Finish batch operation.
      watchdog('commerceml', 'Importer @importer could not be found, skipping feed clone.',
        array('@importer' => $importer_copy_from_name), WATCHDOG_WARNING);
      $context['finished'] = 1;
      return;
    }
    $importer = feeds_importer($feed_name);
    $importer->copy($importer_copy_from);
    
    // Clone feeds tamper plugin.
    $tamper_plugins = feeds_tamper_load_by_importer($importer_copy_from_name, FALSE);
    foreach ($tamper_plugins as $tamper_plugin) {
      foreach ($tamper_plugin as $old_instance) {
        $new_instance = feeds_tamper_new_instance();
        $new_instance = $old_instance;
        $new_instance->id = str_replace($importer_copy_from_name, $importer->id, $new_instance->id);
        $new_instance->importer = $importer->id;
        $new_instance->type = 'local';
        $new_instance->export_type = NULL;
        unset($new_instance->table);
        unset($new_instance->disabled);
        $tamper_plugins_new[] = $new_instance;
      }
    }

    $context['results'][] = t('Feed importer @type has been created.', array('@type' => $feed_name));
  }
  
  $config = $importer->getConfig();
  
  // Update commerce product type.
  if ($entity_type == 'commerce_product') {
     $config['name'] = 'Product ' . $bundle_name;
     $config['processor']['config']['product_type'] = $bundle_name;
     $config['processor']['config']['bundle'] = $bundle_name;
  }
  
  $count_deleted = 0;
  $count_created = 0;

  // Remove missing fields from Feed importer.
  foreach ($config['processor']['config']['mappings'] as $key => $value) {
    $field_name = $value['target'];
    // Skip if it is not auto created field.
    if (substr($field_name, 0, strlen($field_name_prefix)) != $field_name_prefix) {
      continue;
    }
    // Skip if field exists.
    if (!empty($fields_existing[$field_name])) {
      continue;
    }
    // No such field in this bundle, remove it from Feeds importer.
    $source = $value['source'];
    unset($config['processor']['config']['mappings'][$key]);
    unset($config['parser']['config']['sources'][$source]);
    unset($config['parser']['config']['rawXML'][$source]);
    unset($config['parser']['config']['exp']['debug'][$source]);
    $count_deleted++;
  }

  // Check all imported fields and update Feeds config if needed.
  $max_key = -1;
  foreach ($fields_existing as $field_name => $field) {
    // Check if field is automatically created by field machine name prefix.
    if (substr($field_name, 0, strlen($field_name_prefix)) != $field_name_prefix) {
      continue;
    }
    $field_id = substr($field_name, strlen($field_name_prefix));
    // Check Feed processor mappings.
    $mapping_found = FALSE;
    foreach ($config['processor']['config']['mappings'] as $key => $mapping) {
      if ($mapping['target'] == $field_name) {
        $mapping_found = TRUE;
        break;
      }
    }
    if ($mapping_found) {
      continue;
    }
    // Search for max parser number.
    if ($max_key < 0) {
      $keys = array_keys($config['parser']['config']['sources']);
      foreach ($keys as $key) {
        $key = explode(':', $key);
        $key = intval($key[1]);
        $max_key = max($max_key, $key);
      }
    }
    $max_key++;
    // Add field to Feeds.
    $source = 'xpathparser:' . $max_key;
    $config['processor']['config']['mappings'][$max_key] = array(
      'source' => $source,
      'target' => $field_name,
    );
    $config['parser']['config']['sources'][$source] = $fields[$field_id]['source'];
    $config['parser']['config']['rawXML'][$source] = 0;
    $config['parser']['config']['exp'] = array('errors' => 0, 'debug' => array($source => 0));
    $field_info = field_info_field($field_name);
    switch ($field_info['type']) {
      case 'text':
        $config['processor']['config']['mappings'][$max_key]['unique'] = FALSE;
        break;
      case 'taxonomy_term_reference':
        $config['processor']['config']['mappings'][$max_key]['term_search'] = 2;
        $config['processor']['config']['mappings'][$max_key]['autocreate'] = 0;
        break;
      default:
        drupal_set_message(t('Unknown field type @type.', array('@type' => $field_info['type'])), 'error');
        watchdog($module_name, 'Unknown field type @type.', array('@type' => $field_info['type']), WATCHDOG_ERROR);
    }
    $count_created++;
  }
  $importer->setConfig($config);
  foreach (array('fetcher', 'parser', 'processor') as $type) {
    $importer->$type->setConfig($config[$type]['config']);
  }
  $importer->save();

  // Save new Tamper Plugins.
  foreach ($tamper_plugins_new as $plugin) {
    feeds_tamper_save_instance($plugin);
  }

  if ($count_deleted > 0) {
    $context['results'][] = t('@count fields removed from Feeds importer @name.',
      array('@count' => $count_deleted, '@name' => $feed_name));
  }
  if ($count_created > 0) {
    $context['results'][] = t('@count fields added to Feeds importer @name.',
      array('@count' => $count_created, '@name' => $feed_name));
  }

  // Finish batch operation.
  $context['finished'] = 1;
}

/**
 * Batch operation to update fields configuration.
 */
function commerceml_fields_process($fields, $options, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['count_created'] = 0;
    $context['sandbox']['max'] = count($fields);
  }

  $module_name = $options['module_name'];
  $field_name_prefix = $options['field_name_prefix'];
  $entity_type = $options['entity_type'];
  $bundle_name = $options['bundle_name'];
  $limit = empty($options['fields_limit']) ? 6 : $options['fields_limit'];

  // Check if given bundle exists and clone default if needed.
  if ($options['entity_type'] == 'commerce_product') {
    $commerce_product_type_exists = db_query('SELECT 1 FROM {commerce_product_type} WHERE type = :type', array(':type' => $bundle_name))->fetchField();
    if (!$commerce_product_type_exists) {
      // Create new product type.
      $product_type = array(
        'type' => $bundle_name,
        'name' => $bundle_name,
        'description' => $bundle_name,
        'help' => '',
        'revision' => 0,
        'is_new' => TRUE,
        'multilingual' => FALSE,
      );
      // Set the multingual value for the product type if entity translation is enabled.
      if (module_exists('entity_translation')) {
        variable_set('language_product_type_' . $product_type['type'], $product_type['multilingual']);
      }
      commerce_product_ui_product_type_save($product_type);
      $context['results'][] = t('Product type @type has been created.', array('@type' => $bundle_name));
      
      // Clone field instances.
      $commerceml_product_copy_from = variable_get('commerceml_product_copy_from', 'product');
      $instances = field_info_instances('commerce_product', $commerceml_product_copy_from);
      $existing_instances = field_info_instances('commerce_product', $bundle_name);
      $field_instance_updated = 0;
      $field_instance_created = 0;
      foreach ($instances as &$instance) {
        $instance['bundle'] = $bundle_name;
        if (array_key_exists($instance['field_name'], $existing_instances)) {
          // Update the field if it is already existing otherwise create.
          field_update_instance($instance);
          $field_instance_updated++;
        }
        else {
          field_create_instance($instance);
          $field_instance_created++;
        }
      }
      $context['results'][] = t('@count field instances has been updated for bundle @type.', array('@count' => $field_instance_updated, '@type' => $bundle_name));
      $context['results'][] = t('@count field instances has been created for bundle @type.', array('@count' => $field_instance_created, '@type' => $bundle_name));
    }
  }

  // Delete missing and update existing fields.
  if (empty($context['sandbox']['existing_checked'])) {
    $count_deleted = 0;
    $count_updated = 0;
    $fields_existing = field_info_instances($entity_type, $bundle_name);
    foreach ($fields_existing as $field_name => $field) {
      // Check if field is automatically created by field machine name prefix.
      if (substr($field_name, 0, strlen($field_name_prefix)) != $field_name_prefix) {
        continue;
      }
      $field_id = substr($field_name, strlen($field_name_prefix));
      if (empty($fields[$field_id])) {
        field_delete_instance($field, $field_cleanup = TRUE);
        $context['results'][] = t('@field deleted.', array('@field' => $field_name));
        $count_deleted++;
      }
      else {
        $fields[$field_id]['exists'] = TRUE;
        if ($field['label'] != $fields[$field_id]['name']) {
          $field['label'] = $fields[$field_id]['name'];
          field_update_instance($field);
          $count_updated++;
        }
      }
    }
    if ($count_deleted > 0) {
      $context['results'][] = t('@count fields deleted.', array('@count' => $count_deleted));
    }
    if ($count_updated > 0) {
      $context['results'][] = t('@count fields updated.', array('@count' => $count_updated));
    }
    $context['sandbox']['existing_checked'] = TRUE;
  }

  // Create new fields.
  $count_created = 0;
  $count = 0;
  foreach ($fields as $id => $f) {
    $count++;
    // Skip fields from previous progress run.
    if ($count <= $context['sandbox']['progress']) {
      continue;
    }
    $context['sandbox']['progress']++;
    if (!empty($f['exists'])) {
      continue;
    }
    $field_name = $field_name_prefix . $id;
    $field = array(
      'field_name' => $field_name,
    );
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'label' => $f['name'],
      'bundle' => $bundle_name,
      'required' => FALSE,
    );
    switch ($f['type']) {
      // Text field.
      case 'string':
        $field['type'] = 'text';
        $instance['widget'] = array(
          'type' => 'text_textfield',
        );
        $instance['display'] = array(
          'default' => array('type' => 'text_plain'),
          'teaser' => array('type' => 'hidden'),
        );
        break;

      // Taxonomy reference field.
      case 'taxonomy_reference':
        $field['type'] = 'taxonomy_term_reference';
        if (isset($f['cardinality'])) {
          $field['cardinality'] = $f['cardinality'];
        }
        $field['settings'] = array(
          'allowed_values' => array(
            array(
              'vocabulary' => $f['vocab_machine_name'],
              'parent' => 0,
            ),
          ),
        );
        $instance['widget'] = array(
          'type' => 'options_select',
        );
        $instance['display'] = array(
          'default' => array('type' => 'taxonomy_term_reference_plain'),
          'teaser' => array('type' => 'hidden'),
        );
        break;
      default:
        drupal_set_message(t('Unknown field type @type.', array('@type' => $f['type'])), 'error');
        watchdog($module_name, 'Unknown field type @type.', array('@type' => $f['type']), WATCHDOG_ERROR);
    }
    // Create field if it not exists.
    if (!field_info_field($field['field_name'])) {
      field_create_field($field);
    }
    // Create field instance.
    field_create_instance($instance);
    $count_created++;
    $context['sandbox']['count_created']++;
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    if ($count_created >= $limit) {
      // Stop here till next progress run.
      return;
    }
  }
  if ($context['sandbox']['count_created'] > 0) {
    $context['results'][] = t('@count fields created.', array('@count' => $context['sandbox']['count_created']));
  }
  // Finish batch operation.
  $context['finished'] = 1;
}

/**
 * Batch operation to update taxonomy terms configuration.
 */
function commerceml_terms_process($terms, $options, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($terms);
  }

  $module_name = $options['module_name'];

  $count = 0;
  foreach ($terms as $vocabulary_machine_name => $vocabulary_terms) {
    $count++;
    // Skip vocabs from previous progress run.
    if ($count <= $context['sandbox']['progress']) {
      continue;
    }
    $context['sandbox']['progress']++;
    $vocab = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);

    // Delete missing and update existing terms.
    $count_deleted = 0;
    $count_updated = 0;
    $existing_terms = taxonomy_term_load_multiple(array(), array('vid' => $vocab->vid));
    foreach ($existing_terms as $term) {
      $guid = db_query('SELECT guid FROM {feeds_item} WHERE entity_type = :et AND entity_id = :eid',
        array(':et' => 'taxonomy_term', ':eid' => $term->tid))->fetchField();
      if (empty($guid) || empty($vocabulary_terms[$guid])) {
        //taxonomy_term_delete($term->tid);
        //db_query('DELETE FROM {feeds_item} WHERE entity_type = :et AND entity_id = :eid',
        //  array(':et' => 'taxonomy_term', ':eid' => $term->tid));
        //$count_deleted++;
      }
      else {
        $vocabulary_terms[$guid]['exists'] = TRUE;
        if ($term->name != $vocabulary_terms[$guid]['name']) {
          $term->name = $vocabulary_terms[$guid]['name'];
          taxonomy_term_save($term);
          db_query('UPDATE {feeds_item} SET imported = :time, hash = :hash WHERE entity_type = :et AND entity_id = :eid',
            array(':et' => 'taxonomy_term', ':eid' => $term->tid, ':time' => time(), ':hash' => md5($term->name)));
          $count_updated++;
        }
      }
    }
    if ($count_deleted > 0) {
      $context['results'][] =  t('@count taxonomy terms deleted for vocabulary @name.',
        array('@count' => $count_deleted, '@name' => $vocab->name));
    }
    if ($count_updated > 0) {
      $context['results'][] =  t('@count taxonomy terms updated for vocabulary @name.',
        array('@count' => $count_updated, '@name' => $vocab->name));
    }

    // Create new terms.
    $count_created = 0;
    foreach ($vocabulary_terms as $t) {
      if (!empty($t['exists'])) {
        continue;
      }
      $term = new stdClass();
      $term->name = $t['name'];
      $term->vid = $vocab->vid;
      $status = taxonomy_term_save($term);
      if ($status == SAVED_NEW) {
        // Insert Feeds item to match terms by GUID when importing taxonomy reference.
        db_query('INSERT INTO {feeds_item} SET entity_type = :et, entity_id = :eid, id = :id, feed_nid = :fid, imported = :time, url = :url, guid = :guid, hash = :hash',
          array(':et' => 'taxonomy_term', ':eid' => $term->tid, ':id' => $vocabulary_machine_name, ':fid' => 0, ':time' => time(), ':url' => '', ':guid' => $t['id'], ':hash' => md5($term->name)));
      }
      $count_created++;
    }
    if ($count_created > 0) {
      $context['results'][] =  t('@count taxonomy terms created for vocabulary @name.',
        array('@count' => $count_created, '@name' => $vocab->name));
    }
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      // Stop here till next progress run.
      return;
    }
  }
  // Finish batch operation.
  $context['finished'] = 1;
}

/**
 * Batch operation to update taxonomy vocabularies configuration.
 */
function commerceml_vocabularies_process($vocabularies, $options, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['count_created'] = 0;
    $context['sandbox']['max'] = count($vocabularies);
  }

  $module_name = $options['module_name'];
  $limit = empty($options['vocabularies_limit']) ? 6 : $options['vocabularies_limit'];

  $vocabs = taxonomy_vocabulary_load_multiple(FALSE);

  // Delete missing vocabularies.
  if (empty($context['sandbox']['existing_checked'])) {
    $count_deleted = 0;
    $count_updated = 0;
    foreach ($vocabs as $v) {
      if ($v->module != $module_name) {
        continue;
      }
      $machine_name = $v->machine_name;
      if (empty($vocabularies[$machine_name])) {
        // This will update all Taxonomy fields so that they don't reference the deleted vocabulary.
        // It also will delete fields that have no remaining vocabulary references.
        // All taxonomy terms of the deleted vocabulary will be deleted as well.
        //taxonomy_vocabulary_delete($v->vid);
        // Delete Feeds guids for this vocabulary terms.
        //db_query('DELETE FROM {feeds_item} WHERE entity_type = :et AND id = :id',
        //  array(':et' => 'taxonomy_term', ':id' => $machine_name));
        //$count_deleted++;
      }
      else {
        $vocabularies[$machine_name]['exists'] = TRUE;
        if ($v->name != $vocabularies[$machine_name]['name']) {
          $v->name = $vocabularies[$machine_name]['name'];
          taxonomy_vocabulary_save($v);
          $count_updated++;
        }
      }
    }
    if ($count_deleted > 0) {
      $context['results'][] = t('@count taxonomy vocabularies deleted.', array('@count' => $count_deleted));
    }
    if ($count_updated > 0) {
      $context['results'][] = t('@count taxonomy vocabularies updated.', array('@count' => $count_updated));
    }
    $context['sandbox']['existing_checked'] = TRUE;
  }

  // Create new vocabularies.
  $count = 0;
  $count_created = 0;
  foreach ($vocabularies as $machine_name => $v) {
    $count++;
    // Skip vocabs from previous progress run.
    if ($count <= $context['sandbox']['progress']) {
      continue;
    }
    $context['sandbox']['progress']++;
    if (!empty($v['exists'])) {
      continue;
    }
    $name = $v['name'];
    $vocabulary = new stdClass();
    $vocabulary->name = $name;
    $vocabulary->machine_name = $machine_name;
    $vocabulary->description = t('@name automatically created during import.',
        array('@name' => $name));
    $vocabulary->module = $module_name;
    taxonomy_vocabulary_save($vocabulary);
    $count_created++;
    $context['sandbox']['count_created']++;
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    if ($count_created >= $limit) {
      // Stop here till next progress run.
      return;
    }
  }
  if ($context['sandbox']['count_created'] > 0) {
    $context['results'][] = t('@count taxonomy vocabularies created.', array('@count' => $context['sandbox']['count_created']));
  }

  // Finish batch operation.
  $context['finished'] = 1;
}

/**
 * Batch finish callback.
 */
function commerceml_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = empty($results) ? t('Not modified.') : theme('item_list', array('items' => $results));
    drupal_set_message($message);
    cache_clear_all();
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation.', array('%error_operation' => $error_operation[0]));
    drupal_set_message($message, 'error');
  }
}
